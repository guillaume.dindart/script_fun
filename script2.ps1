# import all csv
$file1 = Import-Csv -Path ./ref1 -Header "email"
$file2 = Import-Csv -Path ./ref2 -Header "email"

$index = Import-Csv -Path ./candidats -Header "email"

$nameList = $file1 + $file2

# preparing tab for people
$in = @()
$out = @()

#processing
for ($prenom = 0; $prenom -lt $index.email.Count; $prenom ++) {
    $inliste = $false

    for ($listN = 0; $listN -lt $nameList.email.Count; $listN ++) {
        if ($index.email[$prenom] -eq $nameList.email[$listN]) {
            $in += $index[$prenom]
            Write-Output "$($nameList.email[$listN]) est dans les deux listes."
            $inliste = $true
            break
        }
        
    }
    if ($inliste -eq $false) {
        $out += $index[$prenom]
    }
}

#output
Write-Output "Names in files: "
$in

Write-Output ""
Write-Output ""
Write-Output "Names out of files: "
$out

Write-Output ""
Write-Output ""
Write-Output "Exporting Results..."
$in | Export-Csv -Path "./in_files.csv"
$out | Export-Csv -Path "./out_files.csv"

Write-Output "All done"